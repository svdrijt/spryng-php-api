<?php

namespace Spryng;

/**
 * Class API
 * @package Spryng
 */

class API
{
    private $gateway = 'www.spryng.nl';
    private $protocol;
    private $username;
    private $password;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function send($object)
    {
        $parameters = $object->_send();

        // add authentication
        $parameters['args']['USERNAME'] = $this->username;
        $parameters['args']['PASSWORD'] = $this->password;

        $result = $this->_send($this->get_gateway() .$parameters['path'], $parameters['args']);

        return $object->process_result($result);
    }

    public function _send($path, $args)
    {
        $path .= '?' . http_build_query($args);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        $data = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status == 200)
        {
            return $data;
        }
        else
        {
            var_dump($data);
            var_dump($status);
        }
    }

    /**
     * Get the available credits
     *
     * @return int
     */
    public function get_credit_amount()
    {
        $result = $this->_send(
            $this->get_gateway() . 'check.php',
            array('username' => $this->username, 'password' => $this->password));

        if(floatval($result) < 0) throw new \Exception("No balance given, username and password correct?", 2000);
        return $result;
    }

    /**
     * Gateway address without http/https protocol.
     *
     * @param $gateway
     */
    private function set_gateway($gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * Get the full gateway address (including protocol)
     *
     * @return string
     */
    private function get_gateway()
    {
        switch($this->protocol)
        {
            default:
                return (stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://') . $this->gateway . '/';
            case 'http':
            case 'https':
                return $this->protocol . '://' . $this->gateway .'/';
        }
    }

    /**
     * Set the protocol
     *
     * @param $protocol
     * @throws Exception
     */
    private function set_protocol($protocol)
    {
        if(preg_match('(http)(https)', strtolower($protocol)))
        {
            $this->protocol = strtolower($protocol);
        }
        else
        {
            throw new \Exception("Incorrect protocol given", 1002);
        }
    }
}
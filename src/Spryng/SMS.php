<?php

namespace Spryng;

/**
 * Class SMS
 * @package Spryng
 */

class SMS
{
    private $reference;
    private $recipient;
    private $sender;
    private $body;
    private $route;
    private $allow_long_messages;

    public function __construct($sender, $body, $recipients = array(), $reference = null, $route='BUSINESS', $allow_long_messages = false)
    {
        $this->set_sender($sender);

        foreach($recipients as $recipient)
        {
            $this->add_recipient($recipient);
        }

        if($reference !== null) $this->reference = $reference;
        $this->set_route($route);
        $this->allow_long_messages($allow_long_messages);

        $this->set_body($body);
    }

    /**
     * Set the sender ID
     *
     * @param $sender_id
     * @throws \Exception
     */
    public function set_sender($sender_id)
    {
        if(preg_match('/^[\d]{1,14}$|^[\w\d]{1,11}$/', $sender_id))
        {
            $this->sender = $sender_id;
        }
        else
        {
            throw new \Exception("Sender ID {$sender_id} is invalid", 2200);
        }
    }

    /**
     * Set the content of the text message
     *
     * @param $text
     * @throws \Exception
     */
    public function set_body($text)
    {
        if($this->allow_long_messages == 1) $max_length = 612;
        else $max_length = 160;

        if(strlen($text) > 1 && strlen($text) <= $max_length)
        {
            $this->body = $text;
        }
        else
        {
            throw new \Exception("Length of the body is out of boundaries. Minimal length = 1, Maximum length = {$max_length}, ".strlen($text)." given", 2300);
        }
    }

    /**
     * Add receipient
     *
     * @param $msisdn
     * @throws \Exception
     */
    public function add_recipient($msisdn)
    {
        if(preg_match('/^[1-9][0-9]{10,14}$/', $msisdn))
        {
            $this->recipient[] = $msisdn;
        }
        else
        {
            throw new \Exception("Recipient number {$msisdn} is invalid", 2100);
        }
    }

    /**
     * Set the Spryng Business, Spryng Economy or Specific User route
     * BUSINESS, ECONOMY, 0-9 (Specific User route)
     *
     * @param string $route
     * @throws Exception
     */
    private function set_route($route)
    {
        if(preg_match('/^[0-9]*$|\bBUSINESS\b|\bECONOMY\b/', $route))
        {
            $this->route = $route;
        }
        else
        {
            throw new \Exception("Incorrect route given", 1001);
        }
    }

    /**
     * Get the Spryng route
     *
     * @return string
     */
    private function get_route()
    {
        return $this->route;
    }

    /**
     * If you wish to send Long SMS
     * When using long sms the system will automatically divide your message into message up to 153 characters per SMS
     *
     * @param bool $boolean
     */
    private function allow_long_messages($boolean = true)
    {
        $this->allow_long_messages = ($boolean ? 1 : 0);
    }

    /**
     * Data used by the Spryng/API _send function
     *
     * @return array
     */
    public function _send()
    {
        return array(
            'path' => 'send.php',
            'args' => array(
                'OPERATION' => 'send',
                'DESTINATION' => implode(',', $this->recipient),
                'SENDER' => $this->sender,
                'ROUTE' => $this->get_route(),
                'BODY' => $this->body,
                'ALLOWLONG' => $this->allow_long_messages
            )
        );
    }

    /**
     * Result after send command
     *
     * @param $result
     * @return bool
     * @throws \Exception
     */
    public function process_result($result)
    {
        switch($result)
        {
            case '1':
                return true;
            case '100':
                throw new \Exception("Missing parameter", $result);
            case '101':
                throw new \Exception("Username too short", $result);
            case '102':
                throw new \Exception("Username too long", $result);
            case '103':
                throw new \Exception("Password too short", $result);
            case '104':
                throw new \Exception("Password too long", $result);
            case '105':
                throw new \Exception("Destination too short", $result);
            case '106':
                throw new \Exception("Destination too long", $result);
            case '107':
                throw new \Exception("Sender too long", $result);
            case '108':
                throw new \Exception("Sender too short", $result);
            case '109':
                throw new \Exception("Body too short", $result);
            case '110':
                throw new \Exception("Body too short", $result);
            case '200':
                throw new \Exception("Security error", $result);
            case '201':
                throw new \Exception("Unknown route", $result);
            case '202':
                throw new \Exception("Route access violation", $result);
            case '203':
                throw new \Exception("Insufficient credits", $result);
            case '800':
                throw new \Exception("Technical error", $result);
        }
    }
}